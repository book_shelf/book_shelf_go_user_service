
CREATE TABLE IF NOT EXISTS "region" (
    "region_id" uuid PRIMARY KEY NOT NULL,
    "title" VARCHAR NOT NULL
);

CREATE TABLE IF NOT EXISTS "district" (
    "district_id" uuid PRIMARY KEY NOT NULL,
    "title" VARCHAR NOT NULL,
    "region_id" uuid references region(region_id)
);

CREATE TABLE IF NOT EXISTS "users" (
    "id" uuid PRIMARY KEY,
    "first_name" VARCHAR NOT NULL,
    "last_name" VARCHAR NOT NULL,
    "email" VARCHAR UNIQUE NOT NULL,
    "key" VARCHAR NOT NULL,
    "secret" VARCHAR unique NOT NULL,
    "region_id" uuid NOT NULL references region(region_id),
    "district_id" uuid NOT NULL references district(district_id),
    "created_at" TIMESTAMP DEFAULT (now()),
    "updated_at" TIMESTAMP DEFAULT (now()),
    "deleted_at" TIMESTAMP
);
