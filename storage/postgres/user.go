package postgres

import (
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"ibron/ibron_go_user_service.git/genproto/user_service"
	"ibron/ibron_go_user_service.git/models"
	"ibron/ibron_go_user_service.git/pkg/helper"
	"ibron/ibron_go_user_service.git/storage"
)

type UserRepo struct {
	db *pgxpool.Pool
}

func NewUserRepo(db *pgxpool.Pool) storage.UserRepoI {
	return &UserRepo{
		db: db,
	}
}

func (c *UserRepo) Create(ctx context.Context, req *user_service.CreateUser) (resp *user_service.UserPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "users" (
				id,
				first_name,
				last_name,
				email,
				key,
				secret,
				region_id,
				district_id,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.FirstName,
		req.LastName,
		req.Email,
		req.Key,
		req.Secret,
		req.RegionId,
		req.DistrictId,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.UserPrimaryKey{UserId: id.String()}, nil
}

func (c *UserRepo) GetByPKey(ctx context.Context, req *user_service.UserPrimaryKey) (*user_service.User, error) {

	query := `
		SELECT
			u.id,
			COALESCE(u.first_name, ''),
			COALESCE(u.last_name, ''),
			COALESCE(u.email, ''),
			COALESCE(u.key, ''),
			COALESCE(u.secret, ''),
			u.region_id,
			u.district_id,
			TO_CHAR(u.created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(u.updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(u.deleted_at, 'YYYY-MM-DD HH24:MI:SS'), ''),
			r.region_id,
			r.title,
			d.district_id,
			d.title,
			d.region_id
		FROM "users" AS u
		JOIN region AS r ON r.region_id = u.region_id
		JOIN district AS d ON d.district_id = d.district_id
		WHERE u.id = $1 AND u.deleted_at is NULL
	`
	var resp user_service.User
	var region_data = user_service.RegionUser{}
	var district_data = user_service.DistrictUser{}

	err := c.db.QueryRow(ctx, query, req.GetUserId()).Scan(
		&resp.UserId,
		&resp.FirstName,
		&resp.LastName,
		&resp.Email,
		&resp.Key,
		&resp.Secret,
		&resp.RegionId,
		&resp.DistrictId,
		&resp.CreatedAt,
		&resp.UpdatedAt,
		&resp.DeletedAt,
		&region_data.RegionId,
		&region_data.Title,
		&district_data.DistrictId,
		&district_data.Title,
		&district_data.RegionId,
	)

	if err != nil {
		return nil, err
	}

	resp.RegionData = &region_data
	resp.DistrictData = &district_data

	return &resp, nil
}

func (c *UserRepo) GetAll(ctx context.Context, req *user_service.GetListUserRequest) (resp *user_service.GetListUserResponse, err error) {

	resp = &user_service.GetListUserResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " AND TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			u.id,
			COALESCE(u.first_name, ''),
			COALESCE(u.last_name, ''),
			COALESCE(u.email, ''),
			COALESCE(u.key, ''),
			COALESCE(u.secret, ''),
			u.region_id,
			u.district_id,
			TO_CHAR(u.created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(u.updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(u.deleted_at, 'YYYY-MM-DD HH24:MI:SS'), ''),
			r.region_id,
			r.title,
			d.district_id,
			d.title,
			d.region_id
		FROM "users" AS u
		JOIN region AS r ON r.region_id = u.region_id
		JOIN district AS d ON d.district_id = d.district_id
		WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.GetSearch()) > 0 {
		filter = " WHERE first_name||' '||last_name ILIKE" + "'%" + req.Search + "%'"
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var user user_service.User
		var region_data user_service.RegionUser
		var district_data user_service.DistrictUser

		err := rows.Scan(
			&resp.Count,
			&user.UserId,
			&user.FirstName,
			&user.LastName,
			&user.Email,
			&user.Key,
			&user.Secret,
			&user.RegionId,
			&user.DistrictId,
			&user.CreatedAt,
			&user.UpdatedAt,
			&user.DeletedAt,
			&region_data.RegionId,
			&region_data.Title,
			&district_data.DistrictId,
			&district_data.Title,
			&district_data.RegionId,
		)

		if err != nil {
			return resp, err
		}

		user.RegionData = &region_data
		user.DistrictData = &district_data

		resp.Users = append(resp.Users, &user)
	}

	return
}

func (c *UserRepo) Update(ctx context.Context, req *user_service.UpdateUser) (rowsAffected int64, err error) {

	query := `
			UPDATE
			    "users"
			SET
				first_name = $1,
				last_name = $2,
				email = $3,
				key = $4,
				secret = $5,
				region_id = $6,
				district_id = $7,
				updated_at = now()
			WHERE
				id = $8`

	result, err := c.db.Exec(ctx, query,
		req.FirstName,
		req.LastName,
		req.Email,
		req.Key,
		req.Secret,
		req.RegionId,
		req.DistrictId,
		req.UserId,
	)

	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *UserRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	fmt.Println(req)

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"users"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	fmt.Println(query)

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *UserRepo) Delete(ctx context.Context, req *user_service.UserPrimaryKey) error {

	// query := `DELETE FROM "users" WHERE id = $1`

	query := `UPDATE users SET deleted_at = now() WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.UserId)

	if err != nil {
		return err
	}

	return nil
}

func (c *UserRepo) Login(ctx context.Context, req *user_service.LoginReq) (*user_service.User, error) {

	query := `
		SELECT
			id,
			COALESCE(first_name, ''),
			COALESCE(last_name, ''),
			COALESCE(email, ''),
			COALESCE(key, ''),
			COALESCE(secret, ''),
			region_id,
			district_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "users"
		WHERE key = $1 AND secret = $2
	`
	var resp user_service.User

	err := c.db.QueryRow(ctx, query, req.GetKey(), req.GetSecret()).Scan(
		&resp.UserId,
		&resp.FirstName,
		&resp.LastName,
		&resp.Email,
		&resp.Key,
		&resp.Secret,
		&resp.RegionId,
		&resp.DistrictId,
		&resp.CreatedAt,
		&resp.UpdatedAt,
		&resp.DeletedAt,
	)

	if err != nil {
		return nil, err
	}

	return &resp, nil
}
