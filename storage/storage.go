package storage

import (
	"context"
	"ibron/ibron_go_user_service.git/genproto/user_service"
	"ibron/ibron_go_user_service.git/models"
)

type StorageI interface {
	CloseDB()
	User() UserRepoI
	Region() RegionRepoI
	District() DistrictRepoI
}

type RegionRepoI interface {
	Create(ctx context.Context, req *user_service.CreateRegion) (resp *user_service.RegionPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.RegionPrimaryKey) (resp *user_service.Region, err error)
	GetAll(ctx context.Context, req *user_service.GetListRegionRequest) (resp *user_service.GetListRegionResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateRegion) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.RegionPrimaryKey) error
}

type DistrictRepoI interface {
	Create(ctx context.Context, req *user_service.CreateDistrict) (resp *user_service.DistrictPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.DistrictPrimaryKey) (resp *user_service.District, err error)
	GetAll(ctx context.Context, req *user_service.GetListDistrictRequest) (resp *user_service.GetListDistrictResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateDistrict) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.DistrictPrimaryKey) error
}

type UserRepoI interface {
	Create(ctx context.Context, req *user_service.CreateUser) (resp *user_service.UserPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.User, err error)
	GetAll(ctx context.Context, req *user_service.GetListUserRequest) (resp *user_service.GetListUserResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateUser) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.UserPrimaryKey) error
	Login(ctx context.Context, req *user_service.LoginReq) (*user_service.User, error)
}
